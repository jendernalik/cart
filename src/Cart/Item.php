<?php

namespace Recruitment\Cart;

class Item
{

    /**
     * @var smalint
     */
    private $quantity = 0;

    /**
     * @var Recruitment\Entity\Product
     */
    private $product;

    public function __construct(\Recruitment\Entity\Product $product, int $quantity)
    {
        if ($product->getMinimumQuantity() > $quantity) {
            throw new \InvalidArgumentException('Quantity Is Too Low');
        }
        $this->setProduct($product);
        $this->setQuantity($quantity);
    }

    public function setQuantity(int $quantity)
    {
        if ($this->getProduct()->getMinimumQuantity() > $quantity) {
            throw new Exception\QuantityTooLowException('Quantity to low');
        }
        $this->quantity = $quantity;
        return $this;
    }

    public function addQuantity(int $quantity)
    {
        $this->quantity += $quantity;
        return $this;
    }

    public function setProduct(\Recruitment\Entity\Product $product)
    {
        $this->product = $product;
        return $this;
    }

    public function getQuantity(): int
    {
        return $this->quantity;
    }

    public function getProduct(): \Recruitment\Entity\Product
    {
        return $this->product;
    }

    public function getTotalPrice(): int
    {
        return $this->product->getUnitPrice() * $this->getQuantity();
    }
}
