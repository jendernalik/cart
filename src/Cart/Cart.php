<?php

namespace Recruitment\Cart;

class Cart
{

    /**
     * @var \Recruitment\Cart\Item[]
     */
    private $items = [];

    /**
     * @param type $id
     * @return type
     */
    public function checkout($id)
    {
        $cart = clone $this;
        $this->setItems([]);

        return (new \Recruitment\Entity\Order($id, $cart));
    }

    /**
     * @param \Recruitment\Entity\Product $product
     * @param int $quantity
     * @return $this
     */
    public function addProduct(\Recruitment\Entity\Product $product, int $quantity = 1)
    {
        if (!$this->addQuantityIfProductIsset($product, $quantity)) {
            $this->addItem((new \Recruitment\Cart\Item($product, $quantity)));
        }
        return $this;
    }

    /**
     * @param \Recruitment\Cart\Item $item
     */
    private function addItem(\Recruitment\Cart\Item $item)
    {
        $this->items[] = $item;
    }

    /**
     * @param \Recruitment\Entity\Product $product
     * @param type $quantity
     * @return boolean
     */
    public function setQuantity(\Recruitment\Entity\Product $product, $quantity)
    {
        $item = $this->getItemByProductIfExist($product);
        if ($item) {
            $item->setQuantity($quantity);
            return true;
        }
        $this->addProduct($product, $quantity);
        return true;
    }

    /**
     * 
     * @param \Recruitment\Entity\Product $product
     * @param int $quantity
     * @return bool
     */
    public function addQuantityIfProductIsset(\Recruitment\Entity\Product $product, int $quantity): bool
    {
        $item = $this->getItemByProductIfExist($product);
        if ($item) {
            $item->addQuantity($quantity);
            return true;
        }
        return false;
    }

    /**
     * @param type $product
     * @return \Recruitment\Cart\Item|null
     */
    private function getItemByProductIfExist($product): ?\Recruitment\Cart\Item
    {
        foreach ($this->getItems() as $key => $item) {
            if ($item->getProduct()->getId() == $product->getId()) {
                return $this->items[$key];
            }
        }
        return null;
    }

    /**
     * @param int $key
     * @return void
     */
    public function unsetItem(int $key): void
    {
        $items = $this->getItems();
        unset($items[$key]);
        $this->setItems($items);
    }

    public function setItems(array $items): void
    {
        $this->items = array_merge($items);
    }

    public function indexItems(int $keys): void
    {
        $this->setItems(array_merge($this->getItems()));
    }

    public function removeProduct(\Recruitment\Entity\Product $product)
    {
        foreach ($this->getItems() as $key => $item) {
            if ($item->getProduct()->getId() == $product->getId()) {
                $this->unsetItem($key);
                break;
            }
        }
        return $this;
    }

    public function getItems(): array
    {
        return $this->items;
    }

    public function getDataForView(): array
    {
        $data = [];
        /** @var \Recruitment\Cart\Item $item * */
        foreach ($this->getItems() as $k => $item) {
            $data[] = ['id' => $k + 1, 'total_price' => $item->getTotalPrice(), 'quantity' => $item->getQuantity()];
        }
        return $data;
    }

    public function getItem($item): \Recruitment\Cart\Item
    {
        if (isset($this->getItems()[$item])) {
            return $this->getItems()[$item];
        }

        throw new \OutOfBoundsException('Item not exist');
    }

    public function getTotalPrice(): int
    {
        $totalPrice = 0;
        /** @var \Recruitment\Cart\Item  $item * */
        foreach ($this->getItems() as $item) {
            $totalPrice += $item->getTotalPrice();
        }
        return $totalPrice;
    }
}
