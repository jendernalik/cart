<?php

namespace Recruitment\Entity;

class Product
{

    /**
     * @var int
     */
    private $id = null;

    /**
     * @var int
     */
    private $unitPrice = 0;

    /**
     * @var tinyint
     */
    private $minimumQuantity = 1;

    public function setId(int $id)
    {
        $this->id = $id;
        return $this;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setMinimumQuantity(int $minimumQuantity)
    {
        $this->checkInvalidMinimumQuantity($minimumQuantity);
        $this->minimumQuantity = $minimumQuantity;
        return $this;
    }

    private function checkInvalidMinimumQuantity($minimumQuantity)
    {
        if ($minimumQuantity < 1) {
            throw new \InvalidArgumentException('Invalid Minimum Quantity');
        }
    }

    public function getMinimumQuantity(): int
    {
        return $this->minimumQuantity;
    }

    public function setUnitPrice(int $unitPrice)
    {
        if ($unitPrice < 1) {
            throw new \Recruitment\Entity\Exception\InvalidUnitPriceException('Invalid unit price');
        }
        $this->unitPrice = $unitPrice;
        return $this;
    }

    public function getUnitPrice(): int
    {
        return $this->unitPrice;
    }
}
