<?php

namespace Recruitment\Entity;

class Order
{

    /**
     * @var int
     */
    private $id;

    /**
     * 
     * @var \Recruitment\Cart\Cart
     */
    private $cart;

    public function __construct($id, $cart)
    {
        $this->id = $id;
        $this->cart = $cart;
    }

    public function getDataForView(): array
    {
        return [
            'id' => $this->getId(),
            'items' => $this->getCart()->getDataForView(),
            'total_price' => $this->getCart()->getTotalPrice()
        ];
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getCart(): \Recruitment\Cart\Cart
    {
        return $this->cart;
    }
}
